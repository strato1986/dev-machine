#include nginx

rbenv::install { "vagrant":
  group => 'vagrant',
  home  => '/home/vagrant'
}

rbenv::compile { "1.9.3-p327":
  user => "vagrant",
  home => "/home/vagrant",
}